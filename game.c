#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "map.h"

// TODO if continued: Add the end game condition and have non monster encounters.

int main() {
    map overworld;
    initMap(&overworld);

    player p1;
    initPlayer(&p1, 1, 1, 1, 1, 3);

    // Put monster declarations here to build the map

    char intro1[512];
    strcpy(intro1, "You see a man in the distance, hunched on a stone. You draw near and see that he is little more than skin and bones. Please, he begs, I’m so terribly hungry.\n");
    char fight1[512];
    strcpy(fight1, "You lash out with your spear, catching the man in the throat. Blood gurgles through the wound, staining the dirt red. His mouth hangs open, hungry even in the end.\n");
    char no_fight1[512];
    strcpy(no_fight1, "You lash out with your spear, aiming for the man’s throat. With a speed and strength you didn’t think possible, he catches your weapon and lunges, grabbing you by the shoulders and holding you as his body grows plump and yours withers away to nothing.\n"); 
    char trick1[512];
    strcpy(trick1, "This is all I have. You hold out a bait fish from your pack. He doesn’t even thank you before tearing the fish from your hand, eating greedily - gnashing its silver body between his teeth. He fattens before your eyes, and smiles gratefully as he goes.\n");
    char no_trick1[512];
    strcpy(no_trick1, "Sorry, I don’t have anything, you lie, guarding the bait fish in your pack. Lies! he snarls, lunging forward with surprising speed and strength. He grips your shoulders, pinning you. The man grows plump as you wither away, dying of hunger.\n");
    char flee1[512];
    strcpy(flee1, "You run far and fast, leaving the Man of Famine behind you.\n");
    monster m1;
    initMonster(&m1, 3, 1, 0, 1, intro1, fight1, no_fight1, trick1, no_trick1, flee1);
    overworld.tiles[3][1] = 'M';
    overworld.monsters[3][1] = &m1;

    char intro2[512];
    strcpy(intro2, "You are walking through the woods when you come across a clearing full of large red toadstools. A small, rough looking man in a red hat looks up from the mushroom in front of him, Come to see my garden have you? Well, go ahead.\n");
    char fight2[512];
    strcpy(fight2, "You stride through the mushrooms, crushing them underfoot as the man hisses, revealing bloody teeth. You drive your spear through his chest, and as he bleeds the blood is sucked away. You see a slick trail of red behind you, and the clearing smells of iron.\n");
    char no_fight2[512];
    strcpy(no_fight2, "You stride through the mushrooms, crushing them underfoot as the man hisses, revealing bloody teeth. His small, dense form slams into you knocking you to the ground. Red fluid gushes up around you. You smell iron as the man slits your throat.\n");
    char trick2[512];
    strcpy(trick2, "You tiptoe through the toadstools. As you get close to the man, you see him take a small knife from his pocket, and you quickly knock off his hat. He screams and runs from the clearing, leaving a slick trail of red behind him. The clearing smells of iron.\n");
    char no_trick2[512];
    strcpy(no_trick2, "You tiptoe through the toadstools. Just as you come within distance to the mans hat from his head, he darts out a hand and slashes you open. You fall, and red fluid gushes from the mushrooms you crush. You smell iron as your blood waters the ground.\n");
    char flee2[512];
    strcpy(flee2, "You run far and fast, leaving the Red Cap behind you.\n");
    monster m2;
    initMonster(&m2, 2, 7, 1, 0, intro2, fight2, no_fight2, trick2, no_trick2, flee2);
    overworld.tiles[2][7] = 'M';
    overworld.monsters[2][7] = &m2;

    char intro3[512];
    strcpy(intro3, "You come across a placid lake. A beautiful, green-bridled mare drinks from its waters. As you approach, it raises its head and speaks, Are you lost, little one? Climb on my back and I will ferry you to safety.\n");
    char fight3[512];
    strcpy(fight3, "You raise up your spear, thrusting at the creature's heart. Your point lands true and the horse shrieks, melting into nothing but turf and dark jelly. It smells of algae and rotten flesh.\n");
    char no_fight3[512];
    strcpy(no_fight3, "You raise up your spear, thrusting at the creature's heart, but its sharp teeth sink into your arm. As it drags you beneath the water, the mares skin blackens and peels into rotting ribbons, revealing bloated flesh and algae-coated bones. \n");
    char trick3[512];
    strcpy(trick3, "Well I am lost, and my feet are weary,you say. The mare approaches, but instead of mounting it, you tear off its bridle and cast it to the ground. The horse shrieks, and melts into nothing but turf and dark jelly. It smells of algae and rotten flesh.\n");
    char no_trick3[512];
    strcpy(no_trick3, "Well I am lost, and my feet are weary, you say. The mare approaches. You reach to tear off its bridle, but your fingers stick to its skin. The mare laughs as it drags you under, its skin peeling away to reveal bloated flesh and algae-coated bones. \n");
    char flee3[512];
    strcpy(flee3, "You run far and fast, leaving the Water Horse behind you.\n");
    monster m3;
    initMonster(&m3, 5, 8, 2, 1, intro3, fight3, no_fight3, trick3, no_trick3, flee3);
    overworld.tiles[5][8] = 'M';
    overworld.monsters[5][8] = &m3;

    char intro4[512];
    strcpy(intro4, "You come across a wide, shallow creek. An old, hunched woman stand in the waters, wringing out a pure white sheet. She calls out when she sees you, Oh, you there, can you please help me ring out the laundry? My old hands are too feeble.\n");
    char fight4[512];
    strcpy(fight4, "You wade into the water, but instead of taking up an end of the sheet you stab the old woman, tearing open her belly. She wails with fury as she falls back into the stream. Her laundry floats away like a pale ghost.\n");
    char no_fight4[512];
    strcpy(no_fight4, "You wade into the water, but instead of taking up an end of the sheet you lash out at the croneâ€™s stomach. With cunning reflex she wraps the sheets around you, binding your hands. She begins to wring the sheets with you inside, squeezing the life from your body.\n");
    char trick4[512];
    strcpy(trick4, "Of course, you take up a side of the sheet. As she begins to twist, you twist in the same direction, unraveling the twist of the sheet until it begins to twist the other way, entangling the womans hands. She wails with fury as you leave her behind.n");
    char no_trick4[512];
    strcpy(no_trick4, "Of course, you take up a side of the sheet. As she begins to twist, you twist in the opposite direction. Your hands are caught up in the sheet, and you cannot escape as she wraps you up, wringing the life and blood from your body.\n");
    char flee4[512];
    strcpy(flee4, "You run far and fast, leaving the Washer at the Ford behind you.\n");
    monster m4;
    initMonster(&m4, 6, 14, 1, 2, intro4, fight4, no_fight4, trick4, no_trick4, flee4);
    overworld.tiles[6][14] = 'M';
    overworld.monsters[6][14] = &m4;

    char intro5[512];
    strcpy(intro5, "As you are walking along the banks of the river, you hear the sound of hoof beats approaching from the road far behind you. Turning and squinting, you can just barely see that a headless rider is galloping towards you, its black cape blowing in the wind.\n");
    char fight5[512];
    strcpy(fight5, "You stand your ground, heart pounding in time with the beat of hooves. You grip your spear with white knuckles as the rider charges, couching its lance. You stab forward, lodging your spear in the horses flank. The horse and rider turn to black smoke.\n");
    char no_fight5[512];
    strcpy(no_fight5, "You stand your ground, heart pounding in time with the beat of hooves. You grip your spear with white knuckles as the rider charges, couching its lance. You realize your mistake too late. You die skewered on the lance, held aloft in the bitter wind.\n");
    char trick5[512];
    strcpy(trick5, "You spy a bridge in the distance and run for it, willing your legs move faster and faster, the beating of the horseman’s hooves drawing upon you. At the last moment you reach the bridge and dash across. The horse rears back, and the rider turns away.\n");
    char no_trick5[512];
    strcpy(no_trick5, "You spy a bridge in the distance and run for it, willing your legs move faster and faster, the beating of the horsemans hooves drawing upon you. At the last moment you reach the bridge and dash across. The horse rears back, and the rider turns away.\n");
    char flee5[512];
    strcpy(flee5, "You run far and fast, leaving the Headless Horseman behind you.\n");
    monster m5;
    initMonster(&m5, 8, 19, 3, 2, intro5, fight5, no_fight5, trick5, no_trick5, flee5);
    overworld.tiles[8][19] = 'M';
    overworld.monsters[8][19] = &m5;

    printf("\nControls:\n");
    printf("    type exit to exit\n");
    printf("    type help to see this again\n");
    printf("    type key to see a description of the map's symbols\n");
    printf("    type stats to see your current stats\n");
    printf("    type w to move up or s to move down\n");
    printf("    type a to move left or d to move right\n\n");

    printf("Your stats are: %d fight, %d trick and %d health\n\n", p1.fight, p1.trick, p1.health);

    sleep(1);

    printf("You are a fisher, seeking the greatest catch of all. You rise and fall in your rowboat, the sea sparing you no more mercy than it would spare a cast of piece of driftwood.\n");
    printf("But unlike other flotsam, you do not drift aimlessly. You are being tugged through the waves at considerable speeds, pulled along by the prize you seek - the salmon of wisdom.\n");
    printf("You grip your fishing rod with white knuckles as the fish pulls you through the storm.\n");
    printf("Suddenly, a island looms before you, and before long you are being dragged up a river, the salmon cutting easily through the stream. \n");
    printf("Your hands ache with strain and popped blisters, and you won't be able to hold on much longer.\n");
    printf("The river widens, and you find yourself being towed into the middle of a perfectly circular lake surrounded by lush forests.\n");
    printf("As you sit, dumbfounded by the beauty of the scenery, the salmon dives hard. You don’t have the energy to fight it anymore and the salmon escapes, taking your fishing rod with it.\n");
    printf("Damn! You peer over the side of the boat, gazing into the crystalline waters of the spring. Deep below, you see the salmon circling, but without a rod you are helpless to catch it.\n");
    printf("You have nothing save for bait fish and a spear, but perhaps you could find what you need to make a new rod here on this island.\n");
    printf("The current gently pushes you back towards the river, and you paddle to its bank, setting foot on the shore of this unfamiliar land.\n\n");

    sleep(3);

    movePlayer(&p1, &overworld, 0, 0);
    printf("Heres the map: P is your location and the lines are the edges of the map\n");
    printf("M's are monsters and X's are events\n\n");
    printMap(overworld);

    size_t len = 0;
    int numread;
    char *input = NULL;

    /*main run loop*/
    while (1) {
    /* Print >>> then get the input string */
        printf("\n>>> ");	
        numread = getline(&input, &len, stdin);
        /* This gets rid of the newline at the end */
        input[numread - 1] = '\0';

        /* Check for exit */
        if (!strcmp(input, "exit")) {
            break;
        }
        /* Check for help */
        if (!strcmp(input, "help")) {
            printf("\nControls:\n");
            printf("    type exit to exit\n");
            printf("    type help to see this again\n");
            printf("    type key to see a description of the map's symbols\n");
            printf("    type stats to see your current stats\n");
            printf("    type w to move up or s to move down\n");
            printf("    type a to move left or d to move right\n\n");
        }
        /* Check for key */ 
        if (!strcmp(input, "key")) {
            printf("P is your location and the lines are the edges of the map\n");
            printf("M's are monsters and X's are events\n\n");
        }  
        /* Check for stats */ 
        if (!strcmp(input, "stats")) {
            printf("Your stats are: %d fight, %d trick and %d health\n\n", p1.fight, p1.trick, p1.health);
        }  
        /* Check for player moves, returns 1 if player died during move so we break */
        if (!strcmp(input, "w")) {
            if (movePlayer(&p1, &overworld, -1, 0)) {
                printf("Game over\n");
                break;
            }
            printMap(overworld);
        }
        if (!strcmp(input, "s")) {
            if (movePlayer(&p1, &overworld, 1, 0)) {
                printf("Game over\n");
                break;
            }
            printMap(overworld);
        }
        if (!strcmp(input, "a")) {
            if (movePlayer(&p1, &overworld, 0, -1)) {
                printf("Game over\n");
                break;
            }
            printMap(overworld);
        }
        if (!strcmp(input, "d")) {
            if (movePlayer(&p1, &overworld, 0, 1)) {
                printf("Game over\n");
                break;
            }
            printMap(overworld);
        }
    } 
  
    return 0;
}

