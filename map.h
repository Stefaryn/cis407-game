#ifndef TYPES
#define TYPES

#include <string.h>
#include <stdio.h>

#define ROWS 10
#define COLS 25 

typedef struct player {
    int x;
    int y;
    int fight;
    int trick;
    int health;
} player;

void initPlayer(player *self, int x, int y, int fight, int trick, int health) {
    self->x = x; 
    self->y = y;
    self->fight = fight;
    self->trick = trick;
    self->health = health; 
    return;
}

typedef struct monster {
    int x;
    int y;
    int fight;
    int trick;
    char first_sight[255];
    char player_kills[255];
    char player_no_kills[255];
    char player_tricks[255];
    char player_no_tricks[255];
    char player_flees[255];
} monster;

void initMonster(monster *self, int x, int y, int fight, int trick, char *first_sight, char *player_kills, char *player_no_kills, char *player_tricks, char *player_no_tricks, char *player_flees) {
    self->x = x; 
    self->y = y;
    self->fight = fight;
    self->trick = trick;
    strcpy(self->first_sight, first_sight);
    strcpy(self->player_kills, player_kills);
    strcpy(self->player_no_kills, player_no_kills);
    strcpy(self->player_tricks, player_tricks);
    strcpy(self->player_no_tricks, player_no_tricks);
    strcpy(self->player_flees, player_flees);
    return;
}

typedef struct map {
    char tiles[ROWS][COLS];
    monster *monsters[ROWS][COLS]; 
} map;

void initMap(map *self) {
    int i;
    int j; 
    for (i = 0 ; i < ROWS ; ++i) {
        for (j = 0 ; j < COLS ; ++j) {
            if (i == 0 || i == ROWS-1) {
                self->tiles[i][j] = '-';
            }
            else if (j == 0 || j == COLS-1) {
                self->tiles[i][j] = '|';
            }
            else {
                self->tiles[i][j] = ' ';
                self->monsters[i][j] = NULL;
            }
        }
    }
    return;
}

void printMap(map self) {
    int i;
    int j;
    for (i = 0 ; i < ROWS ; ++i) {
        for (j = 0 ; j < COLS ; ++j) {
            printf("%c", self.tiles[i][j]);
        }
        printf("\n");
    }
    return;
}

/* Returns 1 if player died and 0 otherwise */
int encounterMonster(player *thePlayer, monster *theMonster, int *flee) {

    printf("%s\n", theMonster->first_sight);
    printf("Will you fight the monster (input 0), trick the monster (input 1), or flee (input 2)?\n");
    sleep(1);

    int is_valid = 0;
    char input;

    while (!is_valid) {
        printf(">>> ");
        input = getchar();
        printf("%c\n", input);

        if (input  == '0'){
            is_valid = 1;
            if (thePlayer->fight > theMonster->fight) {
                printf("%s\n", theMonster->player_kills);
                ++thePlayer->fight;
                printf("Your fight leveled up and is now %d\n", thePlayer->fight);
                sleep(1);
            }
            else {
                printf("%s\n", theMonster->player_no_kills);
                --thePlayer->health;
                printf("You took one damage and now have %d health\n", thePlayer->health);
                sleep(1);
            }
        }

        else if (input  == '1'){
            is_valid = 1;
            if (thePlayer->trick > theMonster->trick) {
                printf("%s\n", theMonster->player_tricks);
                ++thePlayer->trick;
                printf("Your trick leveled up and is now %d\n", thePlayer->trick);
                sleep(1);
            }
            else {
                printf("%s\n", theMonster->player_no_tricks);
                --thePlayer->health;
                printf("You took one damage and now have %d health\n", thePlayer->health);
                sleep(1);
            }
        }

        else if (input  == '2'){
            is_valid = 1;
            printf("%s\n", theMonster->player_flees);
            *flee = 1;
        }

        else {
            printf("Invalid input, please enter 0, 1 or 2\n");
        }
    }

    return thePlayer->health <= 0;
}

/* Returns 1 if player died and 0 otherwise */
int movePlayer(player *thePlayer, map *theMap, int xChange, int yChange) {
    theMap->tiles[thePlayer->x][thePlayer->y] = ' ';
    if (thePlayer->x + xChange > 0 && thePlayer->x + xChange < ROWS - 1) {
        thePlayer->x += xChange;
    }
    else {
        printf("Can't move to edge of map\n");
    }
    if (thePlayer->y + yChange > 0 && thePlayer->y + yChange < COLS - 1) {
        thePlayer->y += yChange;
    }
    else {
        printf("Can't move to edge of map\n");
    }

    int dead = 0;
    int flee = 0;
    if (theMap->tiles[thePlayer->x][thePlayer->y] == 'M') {
        dead = encounterMonster(thePlayer, theMap->monsters[thePlayer->x][thePlayer->y], &flee);
    }    

    if (!flee) { 
        theMap->tiles[thePlayer->x][thePlayer->y] = 'P';
    }
    else {
        thePlayer->x -= xChange;
        thePlayer->y -= yChange;
        theMap->tiles[thePlayer->x][thePlayer->y] = 'P';
    }

    return dead;
} 

#endif 

